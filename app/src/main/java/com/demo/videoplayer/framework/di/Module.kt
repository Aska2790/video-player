package com.demo.videoplayer.framework.di

import com.demo.videoplayer.data.InMemoryMediaContentRepository
import com.demo.videoplayer.data.InServicePlayerManager
import com.demo.videoplayer.domain.MediaContentRepository
import com.demo.videoplayer.domain.PlayerManager
import com.demo.videoplayer.ui.player.PlayerViewModel
import com.demo.videoplayer.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val viewModels = module {
    viewModel { HomeViewModel(get()) }
    viewModel { PlayerViewModel(get()) }
}

private val repositories = module {
    single<MediaContentRepository> { InMemoryMediaContentRepository() }
}

private val services = module {
    single<PlayerManager> { InServicePlayerManager(get(), get()) }
}

val diModules = listOf(
    viewModels,
    repositories,
    services
)