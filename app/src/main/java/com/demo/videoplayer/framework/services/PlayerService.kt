package com.demo.videoplayer.framework.services

import android.app.Service
import android.content.Intent
import android.media.AudioManager
import android.os.Binder
import android.util.Log
import com.demo.videoplayer.domain.MediaContent
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.audio.AudioAttributes
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PlayerService : Service(), AudioManager.OnAudioFocusChangeListener {

    //region Properties

    private lateinit var player: SimpleExoPlayer
    private val isPlayingMutable: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val progressMutable: MutableStateFlow<Int> = MutableStateFlow(0)
    private val provider = PlayerProvider()
    private val stateListener = PlayerStateListener()

    private val serviceJob = Job()
    private val serviceScope = CoroutineScope(Dispatchers.Main + serviceJob)

    //endregion

    //region Lifecycle

    override fun onCreate() {
        super.onCreate()

        val audioAttributes = AudioAttributes.Builder()
            .setUsage(C.USAGE_MEDIA)
            .setContentType(C.CONTENT_TYPE_MUSIC)
            .build()

        player = SimpleExoPlayer.Builder(this).build()
        player.playWhenReady = false
        player.seekTo(0, 0)
        player.setAudioAttributes(audioAttributes, true)
        player.addListener(stateListener)
        player.prepare()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onBind(intent: Intent?): Binder {
        return provider
    }

    override fun onDestroy() {
        player.release()
        serviceJob.cancel()
        super.onDestroy()
    }

    override fun onAudioFocusChange(focusChange: Int) {
        Log.d(TAG, "onAudioFocusChange: $focusChange")
    }

    //endregion

    //region Nested

    inner class PlayerProvider : Binder() {

        val isPlaying: StateFlow<Boolean> = isPlayingMutable
        val progress: StateFlow<Int> = progressMutable

        fun pause() {
            player.pause()
        }

        fun play() {
            player.play()
        }

        fun setItem(url: String) {
            player.pause()
            player.clearMediaItems()
            player.setMediaItem(MediaItem.fromUri(url))
        }

    }

    inner class PlayerStateListener : Player.EventListener {
        override fun onIsPlayingChanged(isPlaying: Boolean) {
            serviceScope.launch {
                isPlayingMutable.emit(isPlaying)
                if (isPlaying) {
                    updateProgress()
                }
            }
        }

        override fun onPlaybackStateChanged(state: Int) {
            when (state) {
                Player.STATE_IDLE -> {}
                Player.STATE_READY -> {}
                Player.STATE_ENDED -> {
                    serviceScope.launch {
                        player.pause()
                        player.seekTo(0, 0)
                        progressMutable.emit(0)
                    }
                }
                Player.STATE_BUFFERING -> {}
            }
        }

        override fun onEvents(player: Player, events: Player.Events) {
            Log.d(TAG, "onEvents: events")
        }
    }

    private fun updateProgress() {
        serviceScope.launch {
            while (player.isPlaying) {
                delay(50)
                val progress = (player.currentPosition * 100) / player.duration
                progressMutable.emit(progress.toInt())
            }
        }
    }

    companion object{
        private const val TAG = "PlayerService"
    }
    //endregion
}