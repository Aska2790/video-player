package com.demo.videoplayer.ui.base

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.demo.videoplayer.R


class ErrorDialogFragment : DialogFragment() {

    //region Properties

    private val args: ErrorDialogFragmentArgs by navArgs()

    //endregion

    //region Lifecycle

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setIcon(R.drawable.ic_baseline_error_24)
            .setTitle(args.title)
            .setMessage(args.message)
            .setPositiveButton(R.string.label_button_dialog_positive) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
    }

    //endregion
}