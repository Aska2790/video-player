package com.demo.videoplayer.ui.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.demo.videoplayer.R
import com.demo.videoplayer.databinding.PlayerFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlayerFragment : Fragment() {

    //region Properties

    private val args: PlayerFragmentArgs by navArgs()
    private val viewModel: PlayerViewModel by viewModel()
    private lateinit var binding: PlayerFragmentBinding

    //endregion

    //region Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentBinding = PlayerFragmentBinding.inflate(inflater)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeView()
        initializeViewModel()
    }
    //endregion

    //region Initialization

    private fun initializeView() {
        with(binding) {
            progressBar.max = MAX_PROGRESS_VALUE
            executionImageView.setOnClickListener {
                viewModel.execute()
            }
        }
    }

    private fun initializeViewModel() {
        with(viewModel) {
            updateViewItem(args.mediaId)
            viewState.observe(viewLifecycleOwner, this@PlayerFragment::onViewStateChanged)
            viewItem.observe(viewLifecycleOwner) { viewItem ->
                binding.titleTextView.text = viewItem.title
            }
            isPlay.observe(viewLifecycleOwner) { isPlay ->
                val iconResId = if (isPlay) {
                    R.drawable.ic_baseline_pause_24
                } else {
                    R.drawable.ic_baseline_play_24
                }
                binding.executionImageView.setImageResource(iconResId)
            }
            progress.observe(viewLifecycleOwner) { progress ->
                val value = progress.coerceIn(MIN_PROGRESS_VALUE, MAX_PROGRESS_VALUE)
                binding.progressBar.progress = value
            }
        }
    }

    //endregion

    //region Actions

    private fun onViewStateChanged(viewState: PlayerViewModel.ViewState) {
        when (viewState) {
            is PlayerViewModel.ViewState.Error -> {
                val action = PlayerFragmentDirections.showErrorAction(getString(R.string.title_destination_home), viewState.message)
                findNavController().navigate(action)
            }
            PlayerViewModel.ViewState.GoBack -> {
                findNavController().popBackStack()
            }
            PlayerViewModel.ViewState.Loading -> {
            }
            PlayerViewModel.ViewState.Success -> {
            }

        }
    }

    //endregion

    companion object {
        const val MAX_PROGRESS_VALUE = 100
        const val MIN_PROGRESS_VALUE = 0
    }
}