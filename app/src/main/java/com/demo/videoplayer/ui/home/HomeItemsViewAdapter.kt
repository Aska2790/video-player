package com.demo.videoplayer.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.demo.videoplayer.databinding.MediaItemViewBinding

class MediaItemsViewAdapter(
    private val items: List<HomeViewModel.ViewItem>,
    private val listener: ItemClickListener
) : RecyclerView.Adapter<MediaItemsViewAdapter.ViewHolder>() {

    //region Functions

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(MediaItemViewBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.titleTextView.text = item.title
        holder.descriptionTextView.text = item.description
        holder.container.setOnClickListener {
            listener.onItemClicked(item)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    //endregion

    //region Nested

    class ViewHolder(binding: MediaItemViewBinding) : RecyclerView.ViewHolder(binding.root) {
        val titleTextView = binding.titleTextView
        val descriptionTextView = binding.descriptionTextView
        val container = binding.container
    }

    interface ItemClickListener {
        fun onItemClicked(item: HomeViewModel.ViewItem)
    }

    //endregion
}

