package com.demo.videoplayer.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.demo.videoplayer.R
import com.demo.videoplayer.databinding.ActivityMainBinding

class SingleActivity : AppCompatActivity() {

    //region Properties

    private lateinit var navHostFragment: NavHostFragment
    private var binding: ActivityMainBinding? = null

    //endregion

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)
        binding = activityMainBinding
        initializeNavigation()
        initializeActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!navHostFragment.navController.popBackStack()) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    //endregion

    //region Initialization

    private fun initializeNavigation() {
        navHostFragment =
            (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
    }

    private fun initializeActionBar() {
        val navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    //endregion
}