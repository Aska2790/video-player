package com.demo.videoplayer.ui.player

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.videoplayer.domain.PlayerManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PlayerViewModel(
    private val playerManager: PlayerManager
) : ViewModel() {

    //region Properties

    private val viewStateMutable = MutableLiveData<ViewState>(ViewState.Loading)
    val viewState: LiveData<ViewState> = viewStateMutable

    private val viewItemMutable = MutableLiveData<ViewItem>()
    val viewItem: LiveData<ViewItem> = viewItemMutable

    private val isPlayMutable = MutableLiveData(false)
    val isPlay: LiveData<Boolean> = isPlayMutable

    private val progressMutable = MutableLiveData(0)
    val progress: LiveData<Int> = progressMutable

    //endregion

    init {
        viewModelScope.launch {
            playerManager.isPlaying().collect {
                isPlayMutable.postValue(it)
            }
        }

        viewModelScope.launch {
            playerManager.getProgress().collect {
                progressMutable.postValue(it)
            }
        }
    }

    //region Functions

    fun updateViewItem(viewItemId: Int) = viewModelScope.launch {

        viewStateMutable.postValue(ViewState.Loading)
        val mediaContent = playerManager.preparePlaying(viewItemId)
        if (mediaContent == null) {
            viewStateMutable.postValue(ViewState.GoBack)
            return@launch
        }
        viewItemMutable.postValue(ViewItem(mediaContent.title))
        viewStateMutable.postValue(ViewState.Success)
    }

    fun execute() = viewModelScope.launch {
        val value = isPlay.value ?: false
        if (value) {
            playerManager.pause()
        } else {
            playerManager.play()
        }
    }

    //endregion

    //region Nested

    companion object {
        private const val TAG = "PlayerViewModel"
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Success : ViewState()
        object GoBack : ViewState()
        data class Error(val message: String) : ViewState()
    }

    data class ViewItem(
        val title: String
    )

    //endregion
}