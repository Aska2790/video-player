package com.demo.videoplayer.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.videoplayer.R
import com.demo.videoplayer.databinding.HomeFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(), MediaItemsViewAdapter.ItemClickListener {

    //region Properties

    private val viewModel: HomeViewModel by viewModel()
    private var binding: HomeFragmentBinding? = null

    //endregion

    //region Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentBinding = HomeFragmentBinding.inflate(inflater)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initializeView()
        initializeViewModel()
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    //endregion

    //region Initialization

    private fun initializeView() {
        binding?.let {
            with(it) {
                val context = context ?: return

                val layout = LinearLayoutManager(context)
                viewItemsRecycleView.layoutManager = layout
                viewItemsRecycleView.setHasFixedSize(true)
                viewItemsRecycleView.addItemDecoration(DividerItemDecoration(context, layout.orientation))
                viewItemsRecycleView.adapter = MediaItemsViewAdapter(emptyList(), this@HomeFragment)
            }
        }
    }

    private fun initializeViewModel() {
        with(viewModel) {
            updateViewItems()
            viewState.observe(viewLifecycleOwner, this@HomeFragment::onViewStateChanged)
            viewItems.observe(viewLifecycleOwner, this@HomeFragment::onViewItemsChanged)
        }
    }

    //endregion

    //region Actions

    override fun onItemClicked(item: HomeViewModel.ViewItem) {
        viewModel.showPlayerAction(item)
    }

    private fun onViewStateChanged(viewState: HomeViewModel.ViewState) {
        when (viewState) {
            is HomeViewModel.ViewState.Error -> {
                val action = HomeFragmentDirections.showErrorAction(
                    title = getString(R.string.title_destination_home),
                    message = viewState.message
                )
                findNavController().navigate(action)
            }
            HomeViewModel.ViewState.Loading -> {
            }
            HomeViewModel.ViewState.Success -> {
            }
            is HomeViewModel.ViewState.ShowPlayer -> {
                val action = HomeFragmentDirections.showPlayerAction(viewState.id)
                findNavController().navigate(action)
            }
        }
    }

    private fun onViewItemsChanged(items: (List<HomeViewModel.ViewItem>)?) {
        items?.let { viewItems ->
            val viewAdapter = MediaItemsViewAdapter(viewItems, this@HomeFragment)
            binding?.viewItemsRecycleView?.adapter = viewAdapter
        }
    }

    //endregion

}