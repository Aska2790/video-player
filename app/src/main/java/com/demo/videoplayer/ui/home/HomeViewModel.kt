package com.demo.videoplayer.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.videoplayer.domain.MediaContent
import com.demo.videoplayer.domain.MediaContentRepository
import com.demo.videoplayer.domain.PlayerManager
import kotlinx.coroutines.launch

class HomeViewModel(
    private val playerManager: PlayerManager
) : ViewModel() {

    //region Properties

    private val viewStateMutable = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = viewStateMutable

    private val viewItemsMutable = MutableLiveData<List<ViewItem>>()
    val viewItems: LiveData<List<ViewItem>> = viewItemsMutable

    //endregion

    //region Functions

    fun updateViewItems() {
        viewStateMutable.value = ViewState.Loading
        viewModelScope.launch {
            viewItemsMutable.value = playerManager.getAllContent()
                .map { it.toViewItem() }
                .toList()
            viewStateMutable.value = ViewState.Success
        }
    }

    fun showPlayerAction(viewItem: ViewItem) {
        viewStateMutable.value = ViewState.ShowPlayer(viewItem.id)
    }

    //endregion

    //region Nested

    companion object {
        private const val TAG = "HomeViewModel"
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Success : ViewState()
        data class ShowPlayer(val id: Int) : ViewState()
        data class Error(val message: String) : ViewState()
    }

    data class ViewItem(
        val id: Int,
        val title: String,
        val description: String
    )

    private fun MediaContent.toViewItem(): ViewItem {
        return ViewItem(id, title, description)
    }

    //endregion

}