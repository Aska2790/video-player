package com.demo.videoplayer.domain

data class MediaContent(
    val id: Int,
    val uri: String,
    val title: String,
    val description: String,
    val img: Int,
)