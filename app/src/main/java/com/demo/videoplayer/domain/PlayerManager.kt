package com.demo.videoplayer.domain

import kotlinx.coroutines.flow.StateFlow

interface PlayerManager {

    fun isPlaying(): StateFlow<Boolean>

    fun getProgress(): StateFlow<Int>

    suspend fun getAllContent(): List<MediaContent>

    suspend fun preparePlaying(id: Int): MediaContent?

    suspend fun pause()

    suspend fun play()

}