package com.demo.videoplayer.domain

interface MediaContentRepository {

    fun getAllContent(): List<MediaContent>

    fun getContentById(id: Int): MediaContent?

}