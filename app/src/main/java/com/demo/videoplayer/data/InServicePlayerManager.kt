package com.demo.videoplayer.data

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.demo.videoplayer.domain.MediaContent
import com.demo.videoplayer.domain.MediaContentRepository
import com.demo.videoplayer.domain.PlayerManager
import com.demo.videoplayer.framework.services.PlayerService
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.coroutines.flow.StateFlow

class InServicePlayerManager(
    context: Context,
    private val mediaContentRepository: MediaContentRepository
) : PlayerManager, ServiceConnection {

    //region Properties

    private lateinit var provider: PlayerService.PlayerProvider

    //endregion

    //region Constructors

    init {
        val intent = Intent(context, PlayerService::class.java)
        context.bindService(intent, this, Context.BIND_AUTO_CREATE)
        context.startService(intent)
    }

    //endregion

    //region Functions

    override fun getProgress(): StateFlow<Int> {
        return provider.progress
    }

    override fun isPlaying(): StateFlow<Boolean> {
        return provider.isPlaying
    }

    override suspend fun preparePlaying(id: Int): MediaContent? {
        val mediaContent = mediaContentRepository.getContentById(id)
        mediaContent?.let {
            provider.setItem(mediaContent.uri)
        }
        return mediaContent
    }

    override suspend fun pause() {
        provider.pause()
    }

    override suspend fun play() {
        provider.play()
    }

    override suspend fun getAllContent(): List<MediaContent> {
        return mediaContentRepository.getAllContent()
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        if (service is PlayerService.PlayerProvider) {
            provider = service
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {

    }

    //endregion


}