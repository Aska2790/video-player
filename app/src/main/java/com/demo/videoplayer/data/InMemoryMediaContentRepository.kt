package com.demo.videoplayer.data

import com.demo.videoplayer.domain.MediaContent
import com.demo.videoplayer.domain.MediaContentRepository

class InMemoryMediaContentRepository : MediaContentRepository {

    //region Properties

    private val items: List<MediaContent>

    //endregion

    //region Constructors

    init {
        items = listOf(
            MediaContent(1, "https://www.nobexpartners.com/stream_example.mp3", "Demo stream", "stream_example", 1),
            MediaContent(2, "https://storage.googleapis.com/exoplayer-test-media-0/Jazz_In_Paris.mp3", "Jazz_In_Paris.mp3", "Exoplayer test media 0", 1),
        )
    }

    //endregion

    //region Functions

    override fun getAllContent(): List<MediaContent> {
        return items
    }

    override fun getContentById(id: Int): MediaContent? {
        return items.firstOrNull { it.id == id }
    }

    //endregion
}